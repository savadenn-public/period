/**
 * Represent a period
 */
export interface Period {
  /**
   * Included start of the period
   */
  start: Date

  /**
   * Excluded end of the period
   */
  end: Date
}
