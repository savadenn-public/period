import { Period, SimplePeriod } from '.'

export class LinkedPeriod extends SimplePeriod {
  /**
   * Internal pointer to following period
   */
  protected _next: LinkedPeriod | null = null

  /**
   * Following period
   */
  get next(): LinkedPeriod | null {
    return this._next
  }

  /**
   * Internal pointer to previous period
   */
  protected _prev: LinkedPeriod | null = null

  /**
   * Previous period
   */
  get prev(): LinkedPeriod | null {
    return this._prev
  }

  /**
   * Add a period to the link list or extend exiting one if need
   * @param period
   */
  add(period: Period): LinkedPeriod {
    if (this.contains(period)) return this

    if (this.isStrictlyBefore(period)) return this.addAfter(period)
    if (this.isStrictlyAfter(period)) return this.addBefore(period)

    // Extend this period
    this.merge(period)
    this.growBefore()
    this.growAfter()
    return this
  }

  /**
   * Add before this one
   * DO NOT USE IT DIRECTLY
   * @param period
   * @private
   */
  private addBefore(period: Period): LinkedPeriod {
    if (this._prev && this._prev.end >= period.start)
      return this._prev.add(period)

    const linkedPeriod = new LinkedPeriod(period)
    linkedPeriod._next = this
    if (this._prev) {
      this._prev._next = linkedPeriod
      linkedPeriod._prev = this._prev
    }
    this._prev = linkedPeriod

    return linkedPeriod
  }

  /**
   * Add after this one
   * DO NOT USE IT DIRECTLY
   * @param period
   * @private
   */
  private addAfter(period: Period): LinkedPeriod {
    // Pass up the list
    if (this._next && this._next.start <= period.end)
      return this._next.add(period)

    const linkedPeriod = new LinkedPeriod(period)
    linkedPeriod._prev = this
    if (this._next) {
      this._next._prev = linkedPeriod
      linkedPeriod._next = this._next
    }
    this._next = linkedPeriod

    return linkedPeriod
  }

  /**
   * Merge with next neighbour if needed
   * @private
   */
  private growAfter() {
    if (this._next && this.merge(this._next)) {
      this._next = this._next._next
      if (this._next) this._next._prev = this
      this.growAfter()
    }
  }

  /**
   * Merge with previous neighbour if possible
   * @private
   */
  private growBefore() {
    if (this._prev && this.merge(this._prev)) {
      this._prev = this._prev._prev
      if (this._prev) this._prev._next = this
      this.growBefore()
    }
  }
}
