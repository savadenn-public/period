import { Period } from '.'

export class SimplePeriod implements Period {
  /**
   * @inheritDoc
   */
  end: Date

  /**
   * @inheritDoc
   */
  start: Date

  /**
   * Period duration in milliseconds
   */
  get duration(): number {
    return this.end.getTime() - this.start.getTime()
  }

  /**
   * If true indicates an empty period (a.k.a a moment)
   */
  get empty(): boolean {
    return this.end.getTime() === this.start.getTime()
  }

  /**
   * @inheritDoc
   * @param period
   */
  constructor(period: Period) {
    this.start = new Date(period.start)
    this.end = new Date(period.end)

    if (isNaN(this.start?.getTime()))
      throw new Error(`Invalid start date ${period.start}`)

    if (isNaN(this.end?.getTime()))
      throw new Error(`Invalid end date ${period.end}`)

    if (this.start.getTime() >= this.end.getTime())
      throw new Error('On a period, start must be before end')
  }

  /**
   * Returns true if this intersect with given period
   * @param period
   */
  intersect(period: Period): boolean {
    return !(this.isBefore(period) || this.isAfter(period))
  }

  /**
   * Returns true if this contains the given period
   * @param period
   */
  contains(period: Period): boolean {
    return (
      period.start.getTime() >= this.start.getTime() &&
      period.end.getTime() <= this.end.getTime()
    )
  }

  /**
   * Returns true if this contains the given period
   * cannot have same start or end
   * @param period
   */
  strictlyContains(period: Period): boolean {
    return (
      period.start.getTime() > this.start.getTime() &&
      period.end.getTime() < this.end.getTime()
    )
  }

  /**
   * Returns true if this is the same as given period
   * @param period
   */
  equals(period: Period): boolean {
    return (
      period.start.getTime() === this.start.getTime() &&
      period.end.getTime() === this.end.getTime()
    )
  }

  /**
   * Returns true if this ends before the given period (end is excluded so it might equal start)
   * @param period
   */
  isBefore(period: Period): boolean {
    return this.end.getTime() <= period.start.getTime()
  }

  /**
   * Returns true if this strictly before the given period
   * @param period
   */
  isStrictlyBefore(period: Period): boolean {
    return this.end.getTime() < period.start.getTime()
  }

  /**
   * Returns true if this ends before the given period (end is excluded so it might equal start)
   * @param period
   */
  isAfter(period: Period): boolean {
    return this.start.getTime() >= period.end.getTime()
  }

  /**
   * Returns true if this ends strictly before the given period
   * @param period
   */
  isStrictlyAfter(period: Period): boolean {
    return this.start.getTime() > period.end.getTime()
  }

  /**
   * Merges period into this one, return false if not possible
   * End is exclusive, if start of one period equals end of another, it means periods stick to together. This one can be extended.
   * @return true if actually change the period
   */
  merge(period: Period): boolean {
    if (
      this.start.getTime() > period.end.getTime() ||
      this.end.getTime() < period.start.getTime()
    )
      return false

    if (this.contains(period)) return true

    if (period.start.getTime() < this.start.getTime()) this.start = period.start
    if (period.end.getTime() > this.end.getTime()) this.end = period.end
    return true
  }

  /**
   * Removes a periods from this one, it might lead to an empty period (start = date)
   *
   * /!\ This only deals with continuous period, it cannot make a "hole" in this period
   * use Range for this purpose
   *
   * @return true if actually change the period
   */
  remove(period: Period): boolean {
    if (
      this.isBefore(period) ||
      this.isAfter(period) ||
      // Period to remove strictly inside current, aka creating a hole
      this.strictlyContains(period)
    )
      return false

    const simplePeriod = new SimplePeriod(period)

    // Remove all : creates empty period
    if (simplePeriod.strictlyContains(this)) {
      this.end = this.start
      return true
    }

    // Remove part of the start
    if (this.start.getTime() >= period.start.getTime()) {
      this.start = period.end
      return true
    }

    // Remove part of the end
    this.end = period.start
    return true
  }

  /**
   * Clones this period
   */
  clone(): SimplePeriod {
    return new SimplePeriod(this)
  }

  /**
   * @inheritDoc
   */
  toJSON(): Partial<SimplePeriod> {
    return { start: this.start, end: this.end }
  }
}
