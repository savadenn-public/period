import { LinkedPeriod, Period, SimplePeriod } from '.'

type Traversable<T> = Iterable<T> & {
  forEach: (callback: (value: T) => void, thisArg?: unknown) => void
}

/**
 * A range of time composed of one or more exclusive period
 */
export class Range implements Partial<Period>, Traversable<LinkedPeriod> {
  /**
   * First continuous period
   * @private
   */
  private head: LinkedPeriod | null = null

  /**
   * Last continuous period
   * @private
   */
  private tail: LinkedPeriod | null = null

  /**
   * @param period
   */
  constructor(period?: Period | null) {
    if (period) this._init(period)
  }

  /**
   * First period of the range
   */
  get first(): LinkedPeriod | null {
    return this.head
  }

  /**
   * Last period of the range
   */
  get last(): LinkedPeriod | null {
    return this.tail
  }

  /**
   * @inheritDoc
   */
  get start(): Date | undefined {
    return this.head?.start
  }

  /**
   * @inheritDoc
   */
  get end(): Date | undefined {
    return this.tail?.end
  }

  /**
   * Some of duration of all periods of this range in milliseconds
   * returns O if any periods
   */
  get duration(): number {
    let duration = 0
    this.forEach((p) => (duration += p.duration))
    return duration
  }

  /**
   * Iterate through periods in reverse order
   */
  get reverse(): Traversable<LinkedPeriod> {
    // Lambda function is not possible in getter
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const range = this
    return {
      /**
       * Executes function on all periods
       */
      forEach(callback: (period: LinkedPeriod) => void): void {
        for (const period of this) {
          callback(period)
        }
      },

      /**
       * @inheritDoc
       */
      *[Symbol.iterator]() {
        let current = range.tail
        while (current) {
          yield current
          current = current.prev
        }
      },
    }
  }

  /**
   * Number of continuous periods
   */
  get size(): number {
    let count = 0
    this.forEach(() => count++)
    return count
  }

  /**
   * Returns a continuous period with same start/end as this one
   */
  get asPeriod(): SimplePeriod | null {
    if (this.start && this.end) {
      return new SimplePeriod({ start: this.start, end: this.end })
    }
    return null
  }

  /**
   * Creates a clone of this range
   */
  clones(): Range {
    const range = new Range(this.head)
    this.forEach((period) => range.addPeriod(period))
    return range
  }

  /**
   * Add a continuous period into this one
   * @param newPeriod
   */
  addPeriod(newPeriod: Period): LinkedPeriod {
    if (!this.tail || !this.head) {
      return this._init(newPeriod)
    }

    if (this.tail.isBefore(newPeriod)) {
      this.tail = this.tail.add(newPeriod)
      return this.tail
    }

    const linkedPeriod = this.head.add(newPeriod)

    if (this.head.isAfter(newPeriod)) {
      this.head = linkedPeriod
    }
    return linkedPeriod
  }

  /**
   * Remove a continuous period from this range
   * @return true if actual remove happens (false means no change)
   */
  removePeriod(period: Period): boolean {
    if (
      !this.tail ||
      !this.head ||
      this.head.isAfter(period) ||
      this.tail.isBefore(period)
    )
      return false

    return this.head.remove(period)
  }

  /**
   * Executes function on all periods
   */
  forEach(callback: (period: LinkedPeriod) => void): void {
    for (const period of this) {
      callback(period)
    }
  }

  /**
   * @inheritDoc
   */
  *[Symbol.iterator](): Iterator<LinkedPeriod> {
    let current: LinkedPeriod | null = this.head
    while (current) {
      yield current
      current = current.next
    }
  }

  /**
   * @inheritDoc
   * @param period
   */
  equals(period: this): boolean {
    if (period.size !== this.size) return false
    let current = period.head
    for (const cPeriod of this) {
      if (!current || !cPeriod.equals(current)) return false
      current = current.next
    }
    return true
  }

  // noinspection JSUnusedGlobalSymbols
  /**
   * @inheritDoc
   */
  toJSON(): Period[] {
    const periods: Period[] = []
    this.forEach((p) => periods.push(p))
    return periods
  }

  /**
   * Checks if given period (or range) is included into this one
   * @param periodOrRange
   */
  contains(periodOrRange: Period | Range): boolean {
    if (periodOrRange instanceof Range) {
      for (const period of periodOrRange) {
        if (!this.contains(period)) return false
      }
      return true
    } else {
      for (const localPeriod of this) {
        if (localPeriod.contains(periodOrRange)) return true
        // Quick escape
        if (localPeriod.isAfter(periodOrRange)) return false
      }
      return false
    }
  }

  /**
   * Initiates Range
   * @private
   */
  private _init(period: Period) {
    this.head = new LinkedPeriod(period)
    this.tail = this.head
    return this.head
  }
}
