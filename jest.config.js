const reporters = [
  'jest-junit',
  [
    'jest-html-reporters',
    { filename: process.env.JEST_HTML_DIRECTORY || '/tmp/output/index.html' },
  ],
]

module.exports = {
  testMatch: ['**/?(*.)+(spec|test).[jt]s?(x)'],

  collectCoverageFrom: ['src/**'],
  reporters,
  coverageReporters: ['lcov', 'cobertura', 'text-summary'],
  coverageThreshold: {
    global: {
      statements: 100,
      branches: 100,
      functions: 100,
      lines: 100,
    },
  },
}
