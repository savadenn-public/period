import { Range } from '../dist'
import { log } from './logger'

log({ message: 'Starting load tests' })

const SIZE = 10000
log({ message: ` ${SIZE}` })

const start = Date.now()
for (let i = 0; i < SIZE; i++) {
  const range = new Range()
  range.addPeriod({
    start: new Date('2020-01-01T00:00:00'),
    end: new Date('2020-01-01T01:00:00'),
  })
  range.addPeriod({
    start: new Date('2020-01-01T02:00:00'),
    end: new Date('2020-01-01T03:00:00'),
  })
  range.addPeriod({
    start: new Date('2020-01-01T06:00:00'),
    end: new Date('2020-01-01T07:00:00'),
  })

  range.contains({
    start: new Date('2020-01-01T02:10:00'),
    end: new Date('2020-01-01T02:55:00'),
  })
}
log({ message: `Done in ${Date.now() - start} ms` })
