import { REFS } from '../refs'
import { Period, Range } from '../../src'

describe('tests Range object', () => {
  it('can be traversed in reverse order', () => {
    const range = new Range(REFS.h00)
    range.addPeriod(REFS.h02)
    range.addPeriod(REFS.h04)

    const periods: Period[] = []

    range.reverse.forEach((p) => periods.push(p))

    expect(JSON.stringify(periods, null, 4)).toEqual(
      JSON.stringify([REFS.h04, REFS.h02, REFS.h00], null, 4)
    )
  })

  it('can be traversed', () => {
    const range = new Range(REFS.h00)
    range.addPeriod(REFS.h02)
    range.addPeriod(REFS.h04)

    const periods: Period[] = []

    range.forEach((p) => periods.push(p))

    expect(JSON.stringify(periods, null, 4)).toEqual(
      JSON.stringify([REFS.h00, REFS.h02, REFS.h04], null, 4)
    )
  })
})
