import { REFS } from '../refs'
import { Range } from '../../src'

describe('tests Range object', () => {
  it('equals itself', () => {
    const range = new Range(REFS.h00)
    expect(range.equals(range)).toBe(true)
  })

  it('equals its clone', () => {
    const range = new Range(REFS.h00)
    expect(range.equals(range.clones())).toBe(true)
  })

  it('is not equals to a different range', () => {
    const range = new Range(REFS.h00)
    const range2 = new Range(REFS.h02)
    expect(range.equals(range2)).toBe(false)
  })

  it('is not equals to a different range', () => {
    const range = new Range(REFS.h00)
    const range2 = new Range(REFS.h00)
    range2.addPeriod(REFS.h02)
    expect(range.equals(range2)).toBe(false)
  })
})
