import { REFS } from '../refs'
import { LinkedPeriod, Range, SimplePeriod } from '../../src'

const comparable = (o: unknown) => JSON.stringify(o, null, 4)

describe('tests Range object', () => {
  it('creates an empty range', () => {
    const range = new Range()
    expect(range.first).toBeNull()
    expect(range.last).toBeNull()
    expect(range.start).toBeUndefined()
    expect(range.end).toBeUndefined()
    expect(range.asPeriod).toBeNull()
    expect(range.duration).toEqual(0)
    expect(range.contains(REFS.h00)).toBe(false)
  })

  it('creates a range', () => {
    const range = new Range(REFS.h00)
    const expectedToString = comparable([REFS.h00])
    expect(comparable(range)).toBe(expectedToString)
    expect(range.first).toEqual(new LinkedPeriod(REFS.h00))
    expect(range.last).toEqual(new LinkedPeriod(REFS.h00))
    expect(range.start).toEqual(REFS.h00.start)
    expect(range.end).toEqual(REFS.h00.end)
    expect(range.asPeriod).toEqual(REFS.h00)
    expect(range.duration).toEqual(3600000)

    // Create empty and add
    const range2 = new Range()
    range2.addPeriod(REFS.h00)
    expect(comparable(range2)).toBe(expectedToString)
  })

  it('clone a range', () => {
    const range = new Range(REFS.h00)
    range.addPeriod(REFS.h02)
    range.addPeriod(REFS.h04)

    const clone = range.clones()
    expect(range).not.toBe(clone)
    expect(clone).toEqual(range)
    expect(comparable(range)).toBe(comparable(clone))
  })

  it('is also a period', () => {
    const range = new Range(REFS.h00)
    range.addPeriod(REFS.h02)
    range.addPeriod(REFS.h04)
    range.addPeriod(REFS.h06)

    expect(range.start).toEqual(REFS.h00.start)
    expect(range.end).toEqual(REFS.h06.end)
  })

  it('converts to simple period', () => {
    const range = new Range(REFS.h00)
    const period = new SimplePeriod(REFS.h00)
    expect(range.asPeriod).toEqual(period)
  })

  it('add a period at the end of the range', () => {
    const range = new Range(REFS.h00)
    range.addPeriod(REFS.h02)
    expect(range.last?.equals(REFS.h02)).toEqual(true)
  })

  it('add a period at start of the range', () => {
    const range = new Range(REFS.h02)
    range.addPeriod(REFS.h00)
    expect(range.first?.equals(REFS.h00)).toEqual(true)
  })
})
