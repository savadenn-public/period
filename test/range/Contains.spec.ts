import { Range, SimplePeriod } from '../../src'
import { REFS } from '../refs'

describe('Range::contains', () => {
  it('contains itself', () => {
    const range = new Range(REFS.h00)
    range.addPeriod(REFS.h02)
    const clone = range.clones()

    expect(JSON.stringify(range)).toEqual(JSON.stringify(clone))

    expect(range.contains(clone)).toBe(true)
    expect(clone.contains(range)).toBe(true)
  })

  it('contains a sub period', () => {
    const range = new Range(REFS.h00)
    range.addPeriod(REFS.h02)

    expect(
      range.contains(
        new SimplePeriod({
          start: new Date('2020-01-01T02:10:00'),
          end: new Date('2020-01-01T02:55:00'),
        })
      )
    ).toBe(true)
  })

  it('does not contain a sub period', () => {
    const range = new Range(REFS.h00)
    range.addPeriod(REFS.h02)

    expect(
      range.contains(
        new SimplePeriod({
          start: new Date('2020-01-01T02:10:00'),
          end: new Date('2020-01-01T03:15:00'), // 15 min overflow
        })
      )
    ).toBe(false)
  })

  it('does not contain a sub period and exit quickly', () => {
    const range = new Range(REFS.h00)
    range.addPeriod(REFS.h02)

    expect(
      range.contains(
        new SimplePeriod({
          start: new Date('2020-01-01T00:10:00'),
          end: new Date('2020-01-01T01:15:00'), // 15 min overflow
        })
      )
    ).toBe(false)
  })

  it('does not contains a non overlapping range', () => {
    const range = new Range(REFS.h00)
    range.addPeriod(REFS.h02)

    const range2 = new Range(REFS.h00)
    range2.addPeriod(REFS.h04)

    expect(range.contains(range2)).toBe(false)
    expect(range2.contains(range)).toBe(false)
  })
})
