import { SimplePeriod } from '../../src'

/**
 * Period test dataset
 *
 * BEFORE      0----------2
 * HEAD             1---------3(START)
 * STARTING               2--------------5
 * REF                        3(START)==========6(END)
 * INSIDE                            4---5
 * ENDING  Not needed, invert REF and STARTING
 * TAIL    Not needed, invert REF and HEAD
 * AFTER   Not needed, invert REF and BEFORE
 */
const getDates = () => [
  new Date('2019-12-30T12:00:00Z'), // O
  new Date('2019-12-31T00:00:00Z'), // 1
  new Date('2019-12-31T12:00:00Z'), // 2
  new Date('2020-01-01T00:00:00Z'), // 3
  new Date('2020-01-01T06:00:00Z'), // 4
  new Date('2020-01-01T12:00:00Z'), // 5
  new Date('2020-01-02T00:00:00Z'), // 6
]

let DATES = getDates()
let START = DATES[3]
let END = DATES[6]

const getPeriods = () => ({
  BEFORE: new SimplePeriod({ start: DATES[0], end: DATES[2] }),
  HEAD: new SimplePeriod({ start: DATES[1], end: START }),
  STARTING: new SimplePeriod({ start: DATES[2], end: DATES[5] }),
  REF: new SimplePeriod({ start: START, end: END }),
  INSIDE: new SimplePeriod({ start: DATES[4], end: DATES[5] }),
})
let PERIODS = getPeriods()

beforeEach(() => {
  DATES = getDates()
  START = DATES[3]
  END = DATES[6]
  PERIODS = getPeriods()
})

describe('SimplePeriod', () => {
  it('clones date at instantiation', () => {
    expect(PERIODS.REF.start).not.toBe(START)
    expect(PERIODS.REF.start).toEqual(START)
    expect(PERIODS.REF.end).not.toBe(END)
    expect(PERIODS.REF.end).toEqual(END)
  })

  it('checks validity of dates', () => {
    expect(
      () => new SimplePeriod({ start: START, end: new Date('foobar') })
    ).toThrowError('Invalid end date Invalid Date')

    expect(
      () => new SimplePeriod({ start: new Date('foobar'), end: END })
    ).toThrowError('Invalid start date Invalid Date')
  })

  it('stringify toJSON and parse from it', () => {
    const json = JSON.stringify(PERIODS.REF)
    expect(json).toBe(
      '{"start":"2020-01-01T00:00:00.000Z","end":"2020-01-02T00:00:00.000Z"}'
    )
    const period = new SimplePeriod(JSON.parse(json))
    expect(period).toEqual(PERIODS.REF)
  })

  it('has equality with clone', () => {
    expect(PERIODS.REF).toEqual(PERIODS.REF.clone())
    expect(PERIODS.REF.equals(PERIODS.REF.clone())).toBe(true)
  })

  it('throws an error if date are not sequential', () => {
    expect(() => new SimplePeriod({ start: END, end: START })).toThrow()
    expect(() => new SimplePeriod({ start: START, end: START })).toThrow()
  })

  it('SimplePeriod.duration', () => {
    const period = new SimplePeriod({
      start: new Date('2019-12-31T00:00:00.150Z'),
      end: new Date('2019-12-31T01:32:00.300+01:30'),
    })

    expect(period.duration).toEqual(120150)
  })

  describe('SimplePeriod.intersect', () => {
    it('tests period before and after REF', () => {
      expect(PERIODS.REF.intersect(PERIODS.BEFORE)).toBe(false)
      expect(PERIODS.BEFORE.intersect(PERIODS.REF)).toBe(false)
    })

    it('tests period heading and tailing REF', () => {
      expect(PERIODS.REF.intersect(PERIODS.HEAD)).toBe(false)
      expect(PERIODS.HEAD.intersect(PERIODS.REF)).toBe(false)
    })

    it('tests period starting and ending REF', () => {
      expect(PERIODS.REF.intersect(PERIODS.STARTING)).toBe(true)
      expect(PERIODS.STARTING.intersect(PERIODS.REF)).toBe(true)
    })

    it('tests period inside REF', () => {
      expect(PERIODS.REF.intersect(PERIODS.INSIDE)).toBe(true)
      expect(PERIODS.INSIDE.intersect(PERIODS.REF)).toBe(true)
    })
  })

  describe('SimplePeriod.contains', () => {
    it('tests period before and after REF', () => {
      expect(PERIODS.REF.contains(PERIODS.BEFORE)).toBe(false)
      expect(PERIODS.BEFORE.contains(PERIODS.REF)).toBe(false)
    })

    it('tests period heading and tailing REF', () => {
      expect(PERIODS.REF.contains(PERIODS.HEAD)).toBe(false)
      expect(PERIODS.HEAD.contains(PERIODS.REF)).toBe(false)
    })

    it('tests period starting and ending REF', () => {
      expect(PERIODS.REF.contains(PERIODS.STARTING)).toBe(false)
      expect(PERIODS.STARTING.contains(PERIODS.REF)).toBe(false)
    })

    it('tests period inside REF', () => {
      expect(PERIODS.REF.contains(PERIODS.INSIDE)).toBe(true)
      expect(PERIODS.INSIDE.contains(PERIODS.REF)).toBe(false)
    })
  })

  describe('SimplePeriod.strictlyContains', () => {
    it('tests period before and after REF', () => {
      expect(PERIODS.REF.strictlyContains(PERIODS.BEFORE)).toBe(false)
      expect(PERIODS.BEFORE.strictlyContains(PERIODS.REF)).toBe(false)
    })

    it('tests period heading and tailing REF', () => {
      expect(PERIODS.REF.strictlyContains(PERIODS.HEAD)).toBe(false)
      expect(PERIODS.HEAD.strictlyContains(PERIODS.REF)).toBe(false)
    })

    it('tests period starting and ending REF', () => {
      expect(PERIODS.REF.strictlyContains(PERIODS.STARTING)).toBe(false)
      expect(PERIODS.STARTING.strictlyContains(PERIODS.REF)).toBe(false)
    })

    it('tests period inside REF', () => {
      expect(PERIODS.REF.strictlyContains(PERIODS.INSIDE)).toBe(true)
      expect(PERIODS.INSIDE.strictlyContains(PERIODS.REF)).toBe(false)
    })
  })

  describe('SimplePeriod.equals', () => {
    it('tests period before and after REF', () => {
      expect(PERIODS.REF.equals(PERIODS.BEFORE)).toBe(false)
      expect(PERIODS.BEFORE.equals(PERIODS.REF)).toBe(false)
    })

    it('tests period heading and tailing REF', () => {
      expect(PERIODS.REF.equals(PERIODS.HEAD)).toBe(false)
      expect(PERIODS.HEAD.equals(PERIODS.REF)).toBe(false)
    })

    it('tests period starting and ending REF', () => {
      expect(PERIODS.REF.equals(PERIODS.STARTING)).toBe(false)
      expect(PERIODS.STARTING.equals(PERIODS.REF)).toBe(false)
    })

    it('tests period inside REF', () => {
      expect(PERIODS.REF.equals(PERIODS.INSIDE)).toBe(false)
      expect(PERIODS.INSIDE.equals(PERIODS.REF)).toBe(false)
    })
  })

  describe('SimplePeriod.isBefore', () => {
    it('tests period before and after REF', () => {
      expect(PERIODS.REF.isBefore(PERIODS.BEFORE)).toBe(false)
      expect(PERIODS.BEFORE.isBefore(PERIODS.REF)).toBe(true)
    })

    it('tests period heading and tailing REF', () => {
      expect(PERIODS.REF.isBefore(PERIODS.HEAD)).toBe(false)
      expect(PERIODS.HEAD.isBefore(PERIODS.REF)).toBe(true)
    })

    it('tests period starting and ending REF', () => {
      expect(PERIODS.REF.isBefore(PERIODS.STARTING)).toBe(false)
      expect(PERIODS.STARTING.isBefore(PERIODS.REF)).toBe(false)
    })

    it('tests period inside REF', () => {
      expect(PERIODS.REF.isBefore(PERIODS.INSIDE)).toBe(false)
      expect(PERIODS.INSIDE.isBefore(PERIODS.REF)).toBe(false)
    })
  })

  describe('SimplePeriod.isStrictlyBefore', () => {
    it('tests period before and after REF', () => {
      expect(PERIODS.REF.isStrictlyBefore(PERIODS.BEFORE)).toBe(false)
      expect(PERIODS.BEFORE.isStrictlyBefore(PERIODS.REF)).toBe(true)
    })

    it('tests period heading and tailing REF', () => {
      expect(PERIODS.REF.isStrictlyBefore(PERIODS.HEAD)).toBe(false)
      expect(PERIODS.HEAD.isStrictlyBefore(PERIODS.REF)).toBe(false)
    })

    it('tests period starting and ending REF', () => {
      expect(PERIODS.REF.isStrictlyBefore(PERIODS.STARTING)).toBe(false)
      expect(PERIODS.STARTING.isStrictlyBefore(PERIODS.REF)).toBe(false)
    })

    it('tests period inside REF', () => {
      expect(PERIODS.REF.isStrictlyBefore(PERIODS.INSIDE)).toBe(false)
      expect(PERIODS.INSIDE.isStrictlyBefore(PERIODS.REF)).toBe(false)
    })
  })

  describe('SimplePeriod.isAfter', () => {
    it('tests period before and after REF', () => {
      expect(PERIODS.REF.isAfter(PERIODS.BEFORE)).toBe(true)
      expect(PERIODS.BEFORE.isAfter(PERIODS.REF)).toBe(false)
    })

    it('tests period heading and tailing REF', () => {
      expect(PERIODS.REF.isAfter(PERIODS.HEAD)).toBe(true)
      expect(PERIODS.HEAD.isAfter(PERIODS.REF)).toBe(false)
    })

    it('tests period starting and ending REF', () => {
      expect(PERIODS.REF.isAfter(PERIODS.STARTING)).toBe(false)
      expect(PERIODS.STARTING.isAfter(PERIODS.REF)).toBe(false)
    })

    it('tests period inside REF', () => {
      expect(PERIODS.REF.isAfter(PERIODS.INSIDE)).toBe(false)
      expect(PERIODS.INSIDE.isAfter(PERIODS.REF)).toBe(false)
    })
  })

  describe('SimplePeriod.isStrictlyAfter', () => {
    it('tests period before and after REF', () => {
      expect(PERIODS.REF.isStrictlyAfter(PERIODS.BEFORE)).toBe(true)
      expect(PERIODS.BEFORE.isStrictlyAfter(PERIODS.REF)).toBe(false)
    })

    it('tests period heading and tailing REF', () => {
      expect(PERIODS.REF.isStrictlyAfter(PERIODS.HEAD)).toBe(false)
      expect(PERIODS.HEAD.isStrictlyAfter(PERIODS.REF)).toBe(false)
    })

    it('tests period starting and ending REF', () => {
      expect(PERIODS.REF.isStrictlyAfter(PERIODS.STARTING)).toBe(false)
      expect(PERIODS.STARTING.isStrictlyAfter(PERIODS.REF)).toBe(false)
    })

    it('tests period inside REF', () => {
      expect(PERIODS.REF.isStrictlyAfter(PERIODS.INSIDE)).toBe(false)
      expect(PERIODS.INSIDE.isStrictlyAfter(PERIODS.REF)).toBe(false)
    })
  })

  describe('SimplePeriod.merge', () => {
    it('tests period before and after REF', () => {
      expect(PERIODS.REF.clone().merge(PERIODS.BEFORE)).toBe(false)
      expect(PERIODS.BEFORE.clone().merge(PERIODS.REF)).toBe(false)
    })

    it('tests period heading and tailing REF', () => {
      const cloneRef = PERIODS.REF.clone()
      expect(cloneRef.merge(PERIODS.HEAD)).toBe(true)

      const clonePeriod = PERIODS.HEAD.clone()
      expect(clonePeriod.merge(PERIODS.REF)).toBe(true)

      // Both clone should be equal
      expect(cloneRef).toEqual(clonePeriod)
      expect(cloneRef.start).toEqual(PERIODS.HEAD.start)
      expect(cloneRef.end).toEqual(PERIODS.REF.end)
    })

    it('tests period starting and ending REF', () => {
      const cloneRef = PERIODS.REF.clone()
      expect(cloneRef.merge(PERIODS.STARTING)).toBe(true)

      const clonePeriod = PERIODS.STARTING.clone()
      expect(clonePeriod.merge(PERIODS.REF)).toBe(true)

      // Both clone should be equal
      expect(cloneRef).toEqual(clonePeriod)
      expect(cloneRef.start).toEqual(PERIODS.STARTING.start)
      expect(cloneRef.end).toEqual(PERIODS.REF.end)
    })

    it('tests period inside REF', () => {
      const cloneRef = PERIODS.REF.clone()
      expect(cloneRef.merge(PERIODS.INSIDE)).toBe(true)
      expect(cloneRef).toEqual(PERIODS.REF)

      const clonePeriod = PERIODS.INSIDE.clone()
      expect(clonePeriod.merge(PERIODS.REF)).toBe(true)

      // Both clone should be equal
      expect(cloneRef).toEqual(clonePeriod)
      expect(cloneRef.start).toEqual(PERIODS.REF.start)
      expect(cloneRef.end).toEqual(PERIODS.REF.end)
    })
  })

  describe('SimplePeriod.remove', () => {
    it('tests period before and after REF', () => {
      expect(PERIODS.REF.remove(PERIODS.BEFORE)).toBe(false)
      expect(PERIODS.BEFORE.remove(PERIODS.REF)).toBe(false)
    })

    it('tests period heading and tailing REF', () => {
      expect(PERIODS.REF.remove(PERIODS.HEAD)).toBe(false)
      expect(PERIODS.HEAD.remove(PERIODS.REF)).toBe(false)
    })

    it('tests period starting and ending REF', () => {
      const ref = PERIODS.REF.clone()
      expect(ref.remove(PERIODS.STARTING)).toBe(true)
      expect(ref).toEqual({
        start: PERIODS.STARTING.end,
        end: PERIODS.REF.end,
      })

      const start = PERIODS.STARTING.clone()
      expect(start.remove(PERIODS.REF)).toBe(true)
      expect(start).toEqual({
        start: PERIODS.STARTING.start,
        end: PERIODS.REF.start,
      })
    })

    it('tests period inside REF', () => {
      expect(PERIODS.REF.remove(PERIODS.INSIDE)).toBe(false)

      const empty = PERIODS.INSIDE.clone()
      expect(empty.remove(PERIODS.REF)).toBe(true)
      expect(empty.empty).toBe(true)
    })
  })
})
