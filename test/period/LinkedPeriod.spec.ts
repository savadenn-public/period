import { LinkedPeriod, Period } from '../../src'
import { REFS } from '../refs'

const createLinkedList = () => {
  const period = new LinkedPeriod(REFS.h00)
  period.add(REFS.h02)
  period.add(REFS.h04)
  period.add(REFS.h06)
  period.add(REFS.h08)
  return period
}

/**
 * Dumps the whole chain into an array
 * @param period
 */
export function dumpLinkedPeriods(period: LinkedPeriod): Period[] {
  const periods = [{ start: period.start, end: period.end }]

  let prev = period.prev
  while (prev) {
    periods.unshift({ start: prev.start, end: prev.end })
    prev = prev.prev
  }
  let next = period.next
  while (next) {
    periods.push({ start: next.start, end: next.end })
    next = next.next
  }

  return periods
}

describe('LinkedPeriod', () => {
  it('LinkedPeriod.constructor', () => {
    const period = new LinkedPeriod(REFS.h00)
    expect(dumpLinkedPeriods(period)).toEqual([REFS.h00])
  })

  it('Add periods to the upper chain', () => {
    const period = new LinkedPeriod(REFS.h00)
    period.add(REFS.h04) // Add next
    expect(dumpLinkedPeriods(period)).toEqual([REFS.h00, REFS.h04])

    period.add(REFS.h02) // Add between those 2
    expect(dumpLinkedPeriods(period)).toEqual([REFS.h00, REFS.h02, REFS.h04])

    period.add(REFS.h06) // Add to last position from 1st
    expect(dumpLinkedPeriods(period)).toEqual([
      REFS.h00,
      REFS.h02,
      REFS.h04,
      REFS.h06,
    ])
  })

  it('Add periods to the lower chain', () => {
    const period = new LinkedPeriod(REFS.h08)
    period.add(REFS.h04) // Add next
    expect(dumpLinkedPeriods(period)).toEqual([REFS.h04, REFS.h08])

    period.add(REFS.h06) // Add between those 2
    expect(dumpLinkedPeriods(period)).toEqual([REFS.h04, REFS.h06, REFS.h08])

    period.add(REFS.h02) // Add to last position from 1st
    expect(dumpLinkedPeriods(period)).toEqual([
      REFS.h02,
      REFS.h04,
      REFS.h06,
      REFS.h08,
    ])
  })

  it('creates a 5 node link', () => {
    const period = createLinkedList()

    expect(dumpLinkedPeriods(period)).toEqual([
      REFS.h00,
      REFS.h02,
      REFS.h04,
      REFS.h06,
      REFS.h08,
    ])
  })

  it('adds a period containing all the linked periods onto the first one', () => {
    const period = createLinkedList()

    const start = new Date('2020-01-01T00:00:00')
    const end = new Date('2020-01-02T00:00:00')

    period.add({ start, end })

    expect(dumpLinkedPeriods(period)).toEqual([{ start, end }])

    expect(period.start).toEqual(start)
    expect(period.end).toEqual(end)

    expect(period.prev).toBeNull()
    expect(period.next).toBeNull()
  })

  it('adds a period containing all the linked periods onto the last one', () => {
    let period = createLinkedList()
    while (period.next) period = period.next

    const start = new Date('2020-01-01T00:00:00')
    const end = new Date('2020-01-02T00:00:00')

    period.add({ start, end })

    expect(dumpLinkedPeriods(period)).toEqual([{ start, end }])

    expect(period.start).toEqual(start)
    expect(period.end).toEqual(end)

    expect(period.prev).toBeNull()
    expect(period.next).toBeNull()
  })

  it('adds a period before middle one', () => {
    const middle = createLinkedList()

    const start = new Date('2020-01-01T00:00:00')
    const end = new Date('2020-01-02T00:00:00')

    middle.add({ start, end })

    expect(middle.prev).toBeNull()
    expect(middle.next).toBeNull()
    expect(middle.start).toEqual(start)
    expect(middle.end).toEqual(end)
  })
})
