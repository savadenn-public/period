export const START = new Date('2020-01-01T00:00:00')
export const END = new Date('2020-01-01T01:00:00')

export const REFS = {
  h00: {
    start: START,
    end: END,
  },
  h02: {
    start: new Date('2020-01-01T02:00:00'),
    end: new Date('2020-01-01T03:00:00'),
  },
  h04: {
    start: new Date('2020-01-01T04:00:00'),
    end: new Date('2020-01-01T05:00:00'),
  },
  h06: {
    start: new Date('2020-01-01T06:00:00'),
    end: new Date('2020-01-01T07:00:00'),
  },
  h08: {
    start: new Date('2020-01-01T08:00:00'),
    end: new Date('2020-01-01T09:00:00'),
  },
}
