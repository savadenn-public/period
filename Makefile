#-----------------------------------------
# Variables
#-----------------------------------------
MKFILE_PATH := $(abspath $(lastword ${MAKEFILE_LIST}))
PROJECT_PATH := $(dir ${MKFILE_PATH})
PROJECT_NAME := $(shell basename ${PROJECT_PATH})
export PROJECT_NAME
PROJECT_URL=${PROJECT_NAME}.docker.localhost
export PROJECT_URL
UID=$(shell id -u)
export UID
GID=$(shell id -g)
export GID

#-----------------------------------------
# Help commands
#-----------------------------------------
.PHONY:
.DEFAULT_GOAL := help

help: ## Prints this help
	@grep -E '^[a-zA-Z_\-\0.0-9]+:.*?## .*$$' ${MAKEFILE_LIST} | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

clean: ## Cleans up environnement
	@docker-compose down --remove-orphans --volumes
	@docker-compose pull || true

sh: ## Open bash in container
	@docker-compose run --rm node bash

dev: clean ## watch change and run tests
	@docker-compose up -d
	@docker-compose ps -q display | xargs docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' | xargs -i  echo "Tests result at http://"{}

build: ## Build the package
	@docker-compose run --rm node bash -c "yarn && yarn lint && yarn build && yarn doc"

test.coverage: ## Test the package and generate coverage report
	@docker-compose run --rm node bash -c "yarn && yarn run test:coverage"

load: ## Load test (Need build)
	@docker-compose run --rm node bash -c "yarn run test:load"
