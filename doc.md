# Period

Represent period and doo the math on it.

## Installation

```shell
yarn add @savadenn/period
```

## Base concept

A [period](./interfaces/period.html) is defined by an **included** start date and an **excluded** end date.

This is by choice.

A date defined a precise point of time but also a small period.
When saying 2020-01-01, it refers to the exact 2020-01-01T00:00:00.000GMT moment or the _whole day_.
It means that a period is often named after its start moment.

So in this library, date are for starting moment. Therefore, a period last until the start of the next period, hence an **exclusive** end date.

## Usage

```typescript
const range = new Range({
  start: new Date('2020-01-01T02:00:00'),
  end: new Date('2020-01-01T03:00:00'),
})
range.addPeriod({
  start: new Date('2020-01-02T02:00:00'),
  end: new Date('2020-01-02T03:00:00'),
})
for (const period of range) {
  console.log(JSON.stringify(period))
}
```

## Tests report

Read test report [here](./testReport.html)
