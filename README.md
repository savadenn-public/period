# Periods

Operations on period.

[Read the docs](https://savadenn-public.gitlab.io/period/)

## Installation

```shell
yarn add @savadenn/period
```

## Development

```shell
make dev
# Open test result / coverage
# Tests result at http://172.26.0.3, Coverage at http://172.26.0.3/coverage/lcov-report/
```

_Currently there is no autoreload of test results, you need to refresh your browser._

## Release

1. Update version in [package.json](./package.json)
2. Create a release within GitLab
3. CI on tag will publish the library

## Issue and contribution

Feel free to open issue to report a bug or request some improvement.
